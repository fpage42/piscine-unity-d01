﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckJump : MonoBehaviour {

    public playerScript_ex01 player;

    void Update()
    {
  //      player.setCanJump(false);
    }

    void OnTriggerStay2D(Collider2D obj)
    {
 //       Debug.Log("in");
        player.setCanJump(true);
    }

    void OnTriggerExit2D(Collider2D obj)
    {
        Debug.Log("leave");
        player.setCanJump(false);
    }
}
