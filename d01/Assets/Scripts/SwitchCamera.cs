﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchCamera : MonoBehaviour {

	public GameObject c1, c2, c3;
	private Rigidbody2D rb1, rb2, rb3;

	void Start () {
		rb1 = c1.GetComponent<Rigidbody2D> ();
		rb2 = c2.GetComponent<Rigidbody2D> ();
		rb3 = c3.GetComponent<Rigidbody2D> ();
	}
	
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			activePlayer (c1, rb1);
		} else if (Input.GetKeyDown (KeyCode.Alpha2)) {
			activePlayer (c2, rb2);
		} else if (Input.GetKeyDown (KeyCode.Alpha3)) {
			activePlayer (c3, rb3);
		} else if (Input.GetKeyDown (KeyCode.R))
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	private void activePlayer (GameObject p, Rigidbody2D rb)
	{
		transform.parent = p.transform;
		transform.position = new Vector3 (p.transform.position.x, p.transform.position.y, -10);
		rb1.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		rb2.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		rb3.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
		rb.constraints &= ~RigidbodyConstraints2D.FreezePositionX;
	}
}
