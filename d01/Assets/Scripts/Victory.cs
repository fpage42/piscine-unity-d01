﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour {

	public CheckObjectives check1;
	public CheckObjectives check2;
	public CheckObjectives check3;
	public string nextMap;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (check1.isValide () && check2.isValide () && check3.isValide ()) {
			Debug.Log ("Victoire !");
			SceneManager.LoadScene (nextMap);
		}
	}
}
