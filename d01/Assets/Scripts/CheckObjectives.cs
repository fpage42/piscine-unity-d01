﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckObjectives : MonoBehaviour {

	public string tag;
	private bool valide = false;
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D obj)
	{
		if (obj.tag == this.tag) {
			Debug.Log(tag + " is Ok !");
			valide = true;
		}
	}

	void OnTriggerExit2D(Collider2D obj)
	{
		if (obj.tag == this.tag) {
			valide = false;
		}
	}

	public bool isValide()
	{
		return valide;
	}
}