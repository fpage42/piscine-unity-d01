﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plateformeMove : MonoBehaviour {

	public float max;

	private bool monte = false;
	private float pos;
	private Vector2 depart;

	void Start()
	{
		depart = transform.position;
	}

	void Update () {
		this.transform.position	= depart + new Vector2(0.0F, pos);
		if (monte)
		{
			if (pos > max)
				monte = false;
			pos += 0.01F;
		}
		else
		{
			if (pos <= 0.0F)
				monte = true;
			pos -= 0.01F;
		}
}
}
