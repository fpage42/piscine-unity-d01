﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript_ex01 : MonoBehaviour {

	public float speed;
	public float jumpValue;
	private Rigidbody2D rb;
    private bool canJump = false;
	void Start () {
		rb = this.GetComponent<Rigidbody2D> ();
	}

	void Update () {
		if (transform.childCount == 2) {
			if (Input.GetKeyUp (KeyCode.RightArrow))
				rb.velocity = new Vector2 (0.0F, rb.velocity.y);
			if (Input.GetKeyUp (KeyCode.LeftArrow))
				rb.velocity = new Vector2 (0.0F, rb.velocity.y);
			if (Input.GetKey (KeyCode.RightArrow))
				rb.velocity += new Vector2 (speed, 0.0F);
			if (Input.GetKey (KeyCode.LeftArrow))
				rb.velocity += new Vector2 (-speed, 0.0F);
			if (Input.GetKeyDown (KeyCode.Space) && canJump) {
				rb.velocity += new Vector2 (0.0f, jumpValue);
			}
		}
	}

    public void setCanJump(bool bol)
    {
        this.canJump = bol;
    }
}
