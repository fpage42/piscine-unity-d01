﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformeChecker : MonoBehaviour {

    private GameObject add = null, rem = null;

	void Start () {
		
	}
	
	void Update () {
        if (add != null)
        {
            Debug.Log("add");
            add.transform.parent = this.transform.parent;
            add = null;
        }
        if (rem != null)
        {
            Debug.Log("rem");
            rem.transform.parent = null;
            rem = null;
        }
    }

    void OnTriggerEnter2D(Collider2D obj)
	{
        if (obj != null && (obj.tag == "red" || obj.tag == "blue" || obj.tag == "yellow") && obj.transform != null)
            add = obj.gameObject;
    }

    void OnTriggerExit2D(Collider2D obj)
	{
        if (obj != null && obj.transform.parent != null && (obj.tag == "red" || obj.tag == "blue" || obj.tag == "yellow") && add != obj.gameObject)
            rem = obj.gameObject;
    }
}
